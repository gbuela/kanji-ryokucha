package com.gbuela.kanjiryokucha.activity;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.gbuela.kanjiryokucha.R;
import com.gbuela.kanjiryokucha.database.KoohiiData;
import com.gbuela.kanjiryokucha.model.Card;
import com.gbuela.kanjiryokucha.model.CardAnswer;
import com.gbuela.kanjiryokucha.model.CardIdList;
import com.gbuela.kanjiryokucha.model.FetchCardsResult;
import com.gbuela.kanjiryokucha.model.ReviewType;
import com.gbuela.kanjiryokucha.model.Yomi;
import com.gbuela.kanjiryokucha.network.KoohiiTask;
import com.gbuela.kanjiryokucha.network.KoohiiTaskHandler;
import com.gbuela.kanjiryokucha.network.task.FetchCardDataTask;
import com.gbuela.kanjiryokucha.readings.ReadingsManager;
import com.gbuela.kanjiryokucha.utils.DrawingView;
import com.gbuela.kanjiryokucha.utils.KanjiVGHelper;
import com.gbuela.kanjiryokucha.utils.KoohiiProgress;
import com.gbuela.kanjiryokucha.utils.animation.AnimationFactory;
import com.gbuela.kanjiryokucha.utils.piechart.PieChart;
import com.gbuela.kanjiryokucha.utils.piechart.PieDetailsItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReviewActivity extends KoohiiActivity
        implements View.OnClickListener {

    public static final String TYPE_PARAM = "type";
    public static final String CARDIDLIST_PARAM = "cardIdList";

    private static final String KEYWORD_PARAM = "keyword";
    private static final String KANJIID_PARAM = "kanjiId";
    private static final String NUM_YES_PARAM = "num_yes";
    private static final String NUM_NO_PARAM = "num_no";

    private static final String STATE_CURRENT_INDEX = "STATE_CURRENT_INDEX";
    private static final String STATE_CARD_ID_LIST = "STATE_CARD_ID_LIST";
    private static final String STATE_CARDS = "STATE_CARDS";
    private static final String STATE_INITIALIZED = "STATE_INITIALIZED";
    private static final String STATE_FINISHED = "STATE_FINISHED";
    private static final String STATE_CARD_FRONT = "STATE_CARD_FRONT";

    static final int CARDS_REMAINING_FETCH_MORE = 5;

    private Fragment frontFragment;
    private Fragment backFragment;

    private Button buttonFlip;
    private Button buttonDone;
    private Button buttonNo;
    private Button buttonHard;
    private Button buttonYes;
    private Button buttonEasy;
    private ViewAnimator animator;
    private Menu mOptionsMenu;
    private ProgressBar progressBar;
    private View answerButtons;

    // State
    private ReviewType mReviewType;
    private CardIdList mCardIdList;
    private ArrayList<Card> mCards;
    private int mCurrentIndex;
    private boolean mInitialized;
    private boolean mFinished;

    // Transient state - not restored
    private boolean mFront;
    private Bitmap bitmap;
    private KoohiiData data;

    public static Intent getReviewActivityIntent(Context context, ReviewType reviewType, CardIdList cardIdList) {
        Intent intent = new Intent(context, ReviewActivity.class);
        intent.putExtra(TYPE_PARAM, reviewType.getCode());
        intent.putIntegerArrayListExtra(CARDIDLIST_PARAM, cardIdList.getCardIds());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        mReviewType = ReviewType.typeFromInt(getIntent().getIntExtra(TYPE_PARAM, 0));
        final ArrayList<Integer> cardIds = getIntent().getIntegerArrayListExtra(CARDIDLIST_PARAM);
        mCardIdList = new CardIdList();
        mCardIdList.setCardIds(cardIds);

        buttonFlip = (Button)findViewById(R.id.buttonFlip);
        buttonDone = (Button)findViewById(R.id.buttonDone);
        animator = (ViewAnimator)findViewById(R.id.viewFlipper);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        if (mReviewType == ReviewType.FREE_RANGE) {
            buttonNo = (Button)findViewById(R.id.buttonNo2);
            buttonYes = (Button)findViewById(R.id.buttonYes2);
            answerButtons = findViewById(R.id.freeAnswerButtons);

            findViewById(R.id.srsAnswerButtons).setVisibility(View.INVISIBLE);
            findViewById(R.id.freeAnswerButtons).setVisibility(View.VISIBLE);
        }
        else {
            buttonNo = (Button)findViewById(R.id.buttonNo);
            buttonHard = (Button)findViewById(R.id.buttonHard);
            buttonYes = (Button)findViewById(R.id.buttonYes);
            buttonEasy = (Button)findViewById(R.id.buttonEasy);
            answerButtons = findViewById(R.id.srsAnswerButtons);

            findViewById(R.id.srsAnswerButtons).setVisibility(View.VISIBLE);
            findViewById(R.id.freeAnswerButtons).setVisibility(View.INVISIBLE);

            buttonEasy.setOnClickListener(this);
            buttonEasy.setBackgroundColor(getResources().getColor(R.color.koohiiBlue));
            buttonHard.setOnClickListener(this);
            buttonHard.setBackgroundColor(getResources().getColor(R.color.koohiiOrange));
        }

        buttonDone.setOnClickListener(this);
        buttonFlip.setOnClickListener(this);
        buttonNo.setOnClickListener(this);
        buttonYes.setOnClickListener(this);

        // Deal with colors incorrectly rendered on galaxy s2
        buttonFlip.setBackgroundColor(getResources().getColor(R.color.koohiiBlue));
        buttonNo.setBackgroundColor(getResources().getColor(R.color.koohiiRed));
        buttonDone.setBackgroundColor(getResources().getColor(R.color.koohiiBlue));
     }

    @Override
    protected void onResume() {
        super.onResume();

        final KoohiiProgress koohiiProgress = (KoohiiProgress)findViewById(R.id.reviewProgress);
        koohiiProgress.setMaximumValue(mCardIdList.getCardIds().size());

        data = new KoohiiData(this);

        if (!mInitialized) {
            initialize();
        }
        else {
            if (!mFinished) {
                animator.setDisplayedChild(mFront? 0 : 1);
                displayCurrentSide();
                displayCurrentCard();
                ((CardFrontFragment)frontFragment).needsRestore = true;
                if (!mFront) {
                    ((CardBackFragment)backFragment).needsRestore = true;
                }
            }
        }

        String title = getString(R.string.reviewing) + " ";
        switch(mReviewType) {
            case DUE:
                title += "Due " + getResources().getString(R.string.cards);
                break;
            case NEW:
                title += "New " + getResources().getString(R.string.cards);
                break;
            case FAILED:
                title += "Restudy " + getResources().getString(R.string.cards);
                break;
        }
        getActionBar().setTitle(title);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        final Card[] cards = Card.CREATOR.newArray(mCards.size());
        for (int i = 0; i < mCards.size(); i++) {
            cards[i] = mCards.get(i);
        }

        outState.putInt(STATE_CURRENT_INDEX, mCurrentIndex);
        outState.putParcelable(STATE_CARD_ID_LIST, mCardIdList);
        outState.putParcelableArray(STATE_CARDS, cards);
        outState.putBoolean(STATE_INITIALIZED, mInitialized);
        outState.putBoolean(STATE_FINISHED, mFinished);
        outState.putBoolean(STATE_CARD_FRONT, mFront);

        super.onSaveInstanceState(outState);

        if (frontFragment != null) { // solve crash #8
            ((CardFrontFragment) frontFragment).saveDrawing();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mCurrentIndex = savedInstanceState.getInt(STATE_CURRENT_INDEX);
        mCardIdList = savedInstanceState.getParcelable(STATE_CARD_ID_LIST);
        mInitialized = savedInstanceState.getBoolean(STATE_INITIALIZED);
        mFinished = savedInstanceState.getBoolean(STATE_FINISHED);
        mFront = savedInstanceState.getBoolean(STATE_CARD_FRONT);
        final Parcelable[] parcelableCards = savedInstanceState.getParcelableArray(STATE_CARDS);
        mCards = new ArrayList<>(mCardIdList.getCardIds().size());
        for (int i = 0; i < parcelableCards.length; i++) {
            mCards.add((Card)parcelableCards[i]);
        }
    }

    private void initialize() {

        mFront = true;

        mCurrentIndex = -1;

        displayCurrentSide();

        mCards = new ArrayList<>();

        new FetchCardDataTask(mCardIdList, 0).execute(ReviewActivity.this, new KoohiiTaskHandler<FetchCardsResult>() {
            @Override
            public void handle(FetchCardsResult result) {
            mCards.addAll(result.getCards());
            mInitialized = true;
            nextCard();
            }
        });
    }

    private void fetchNextBatch() {
        if (mCards.size() < mCardIdList.getCardIds().size()) {
            new FetchCardDataTask(mCardIdList, mCards.size()).execute(ReviewActivity.this, new KoohiiTaskHandler<FetchCardsResult>() {
                @Override
                public void handle(FetchCardsResult result) {
                    mCards.addAll(result.getCards());
                }
            });
        }
    }

    @Override
    public void onClick(View v) {

        if (v == buttonDone) {
            finish();
        }
        else {
            if (!mFront) {
                CardAnswer answer = CardAnswer.UNANSWERED;
                if (v == buttonYes) {
                    answer = CardAnswer.YES;
                }
                else if (v == buttonNo) {
                    answer = CardAnswer.NO;
                }
                else if (v == buttonHard) {
                    answer = CardAnswer.HARD;
                }
                else if (v == buttonEasy) {
                    answer = CardAnswer.EASY;
                }

                if (mCards != null && mCards.size() > mCurrentIndex) {
                    mCards.get(mCurrentIndex).setAnswer(answer);
                    data.update(mCards.get(mCurrentIndex));
                }
            }
            else {
                if (frontFragment != null) ((CardFrontFragment) frontFragment).willFlip();
                if (backFragment != null) ((CardBackFragment) backFragment).willAppear();
            }

            AnimationFactory.flipTransition(animator, AnimationFactory.FlipDirection.RIGHT_LEFT);
            mFront = !mFront;
            displayCurrentSide();

            if (mFront) {
                nextCard();
            }
        }
    }

    private void setAnswerAndContinue(CardAnswer answer) {
        if (mCurrentIndex < mCards.size()) {
            mCards.get(mCurrentIndex).setAnswer(answer);
            data.update(mCards.get(mCurrentIndex));
            if (!mFront) {
                ((CardFrontFragment) frontFragment).willFlip();
                ((CardBackFragment) backFragment).willAppear();
                mFront = true;
                AnimationFactory.flipTransition(animator, AnimationFactory.FlipDirection.RIGHT_LEFT);
            }
            displayCurrentSide();
            nextCard();
        }
    }

    private void previousCard() {
        if (mCurrentIndex > 0) {
            animator.setDisplayedChild(0);
            mFront = true;
            mCurrentIndex--;
            displayCurrentCard();
            displayCurrentSide();
        }
    }

    private void nextCard() {
        if (mCurrentIndex >= mCards.size()-1) {

            mFinished = true;

            if (mOptionsMenu != null) {
                mOptionsMenu.clear();
            }

            final FragmentManager fragmentManager = this.getFragmentManager();

            int numYes = 0;
            int numNo = 0;
            for (Card card : mCards) {
                if (card.getAnswer() == CardAnswer.YES || card.getAnswer() == CardAnswer.EASY) {
                    numYes++;
                }
                else if (card.getAnswer() == CardAnswer.NO) {
                    numNo++;
                }
            }

            Fragment finishFragment = FinishedFragment.newInstance(numYes,numNo);

            if (!isFinishing()) {
                fragmentManager.beginTransaction()
                        .replace(R.id.cardFrontFragment, finishFragment)
                        .commit();
            }

            displayProgress(mCardIdList.getCardIds().size());
            displayDoneButton();
        }
        else {
            mCurrentIndex++;
            displayCurrentCard();
            if (mCurrentIndex == mCards.size() - CARDS_REMAINING_FETCH_MORE) {
                fetchNextBatch();
            }
        }
    }

    private void displayCurrentCard() {
        final FragmentManager fragmentManager = this.getFragmentManager();

        final Card currentCard = mCards.get(mCurrentIndex);

        frontFragment = CardFrontFragment.newInstance(currentCard);
        backFragment = CardBackFragment.newInstance(currentCard);

        if (!isFinishing()) {
            fragmentManager.beginTransaction()
                    .replace(R.id.cardFrontFragment, frontFragment)
                    .replace(R.id.cardBackFragment, backFragment)
                    .commit();
        }
        displayProgress(mCurrentIndex);
    }

    private void displayProgress(int position) {
        final int displayPos = position < mCardIdList.getCardIds().size()? position + 1 : position;
        final KoohiiProgress koohiiProgress = (KoohiiProgress)findViewById(R.id.reviewProgress);
        koohiiProgress.setCurrentValue(position);
        koohiiProgress.setText("Reviewing: " + displayPos + " of " + mCardIdList.getCardIds().size());
    }

    private void displayCurrentSide() {
        answerButtons.setVisibility(mFront? View.GONE : View.VISIBLE);
        buttonFlip.setVisibility(mFront ? View.VISIBLE : View.GONE);
        buttonDone.setVisibility(View.GONE);

        if (mOptionsMenu != null) {
            final MenuItem clearItem = mOptionsMenu.findItem(R.id.action_clear);
            clearItem.setVisible(mFront);
            final MenuItem strokeOrderItem = mOptionsMenu.findItem(R.id.action_strokeorder);
            strokeOrderItem.setVisible(!mFront);
        }
    }

    private void displayDoneButton() {
        answerButtons.setVisibility(View.GONE);
        buttonFlip.setVisibility(View.GONE);
        buttonDone.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_review, menu);
        mOptionsMenu = menu;

        final MenuItem deleteItem = mOptionsMenu.findItem(R.id.action_delete);
        deleteItem.setVisible(mReviewType != ReviewType.FREE_RANGE);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                ((CardFrontFragment)frontFragment).clearDrawing();
                return true;
            case R.id.action_undo:
                previousCard();
                return true;
            case R.id.action_delete:
                setAnswerAndContinue(CardAnswer.DELETE);
                return true;
            case R.id.action_skip:
                setAnswerAndContinue(CardAnswer.SKIP);
                return true;
            case R.id.action_strokeorder:

                final int kanjiId = mCards.get(mCurrentIndex).getKanjiId();

                final KanjiVGHelper helper = new KanjiVGHelper(this);
                final String htmlUrl = helper.createHtmlFile(kanjiId);

                if (htmlUrl != null) {
                    final Dialog dialog = new Dialog(this);
                    dialog.setContentView(R.layout.strokeorder_web);
                    final WebView web = (WebView)dialog.findViewById(R.id.so_webview);
                    web.getSettings().setJavaScriptEnabled(true);
                    web.loadUrl(htmlUrl);
                    dialog.setCancelable(true);
                    dialog.setTitle(getString(R.string.stroke_order_dialog_title));
                    dialog.show();
                }
                else {
                    Toast.makeText(this,
                            "Something failed! Cannot display stroke order for this kanji", Toast.LENGTH_LONG).show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTaskStart(KoohiiTask task) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskEnd(KoohiiTask task) {
        progressBar.setVisibility(View.INVISIBLE);
    }

    public static class FinishedFragment extends Fragment {

        public static FinishedFragment newInstance(int numYes, int numNo) {
            FinishedFragment fragment = new FinishedFragment();
            Bundle args = new Bundle();
            args.putInt(NUM_YES_PARAM, numYes);
            args.putInt(NUM_NO_PARAM, numNo);
            fragment.setArguments(args);
            return fragment;
        }

        public FinishedFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.fragment_card_finished, container, false);

            if(getArguments() != null) {

                final int numYes = getArguments().getInt(NUM_YES_PARAM);
                final int numNo = getArguments().getInt(NUM_NO_PARAM);
                final float percentYes = (100f / (numYes + numNo)) * numYes;

                final List<PieDetailsItem> piedata=new ArrayList<>(2);
                PieDetailsItem item = new PieDetailsItem();
                item.count = numNo;
                item.color = getResources().getColor(R.color.koohiiRed);
                piedata.add(item);

                item = new PieDetailsItem();
                item.count = numYes;
                item.color = getResources().getColor(R.color.koohiiGreen);
                piedata.add(item);

                final ImageView pie = (ImageView)view.findViewById(R.id.imageViewPieChart);
                final Bitmap mBaggroundImage = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                final PieChart piechart = new PieChart(getActivity());
                piechart.setLayoutParams(new LinearLayout.LayoutParams(200, 200));
                piechart.setGeometry(200, 200, 2, 2, 2, 2, 2130837504);
                piechart.setSkinparams(getResources().getColor(android.R.color.transparent));
                piechart.setData(piedata, numYes + numNo);
                piechart.invalidate();
                piechart.draw(new Canvas(mBaggroundImage));
                pie.setImageBitmap(mBaggroundImage);

                final TextView textYes = (TextView)view.findViewById(R.id.textYesCount);
                final TextView textNo = (TextView)view.findViewById(R.id.textNoCount);
                final TextView textRemembered = (TextView)view.findViewById(R.id.textRemembered);

                final int intPercentYes = (int)Math.ceil(percentYes);
                textRemembered.setText("" + intPercentYes + getResources().getString(R.string.percent_remembered_message));

                textYes.setText("" + numYes);
                textNo.setText("" + numNo);
            }

            return view;
        }

    }

    /**
     * A fragment representing the front of the card.
     */
    public static class CardFrontFragment extends Fragment {

        private DrawingView drawingView;
        public boolean needsRestore;

        public static CardFrontFragment newInstance(Card card) {
            CardFrontFragment fragment = new CardFrontFragment();
            Bundle args = new Bundle();
            args.putString(KEYWORD_PARAM, card.getKeyword());
            args.putInt(KANJIID_PARAM, card.getKanjiId());
            fragment.setArguments(args);
            return fragment;
        }

        public CardFrontFragment() {
            // Required empty public constructor
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_card_front, container, false);

            if(getArguments() != null) {
                TextView keywordText = (TextView)view.findViewById(R.id.frontKeyword);
                keywordText.setText(getArguments().getString(KEYWORD_PARAM));
            }

            drawingView = (DrawingView)view.findViewById(R.id.drawingView);
            return view;
        }

        @Override
        public void onResume() {
            super.onResume();

            if (needsRestore) {
                drawingView.restore();
                restoreBitmap();
                needsRestore = false;
            }
        }

        public void clearDrawing() {
            drawingView.clear();
        }

        public void willFlip() {
            final ReviewActivity activity = (ReviewActivity)getActivity();
            activity.bitmap = Bitmap.createBitmap(drawingView.getWidth(), drawingView.getHeight(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(activity.bitmap);
            drawingView.draw(canvas);
            saveBitmap();
        }

        public void saveBitmap() {
            final ReviewActivity activity = (ReviewActivity)getActivity();
            final File file = new File(activity.getCacheDir(), "bitmap");

            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file);
                activity.bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (Exception e) {
                // fail silently
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    // fail silently
                }
            }
        }

        public void restoreBitmap() {
            final ReviewActivity activity = (ReviewActivity)getActivity();
            final File file = new File(activity.getCacheDir(), "bitmap");

            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                activity.bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            } catch (Exception e) {
                // fail silently
            }
        }

        public void saveDrawing() {
            drawingView.save();
        }
    }

    /**
     * A fragment representing the back of the card.
     */
    public static class CardBackFragment extends Fragment {
        public boolean needsRestore;

        public static CardBackFragment newInstance(Card card) {
            CardBackFragment fragment = new CardBackFragment();
            Bundle args = new Bundle();
            args.putString(KEYWORD_PARAM, card.getKeyword());
            args.putInt(KANJIID_PARAM, card.getKanjiId());
            fragment.setArguments(args);
            return fragment;
        }

        public CardBackFragment() {
            // Required empty public constructor
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_card_back, container, false);

            if(getArguments() != null) {
                final TextView keywordText = (TextView)view.findViewById(R.id.backKeyword);
                keywordText.setText(getArguments().getString(KEYWORD_PARAM));

                final int kanjiId = getArguments().getInt(KANJIID_PARAM);
                final TextView kanjiText = (TextView)view.findViewById(R.id.kanji);
                kanjiText.setText(new String(Character.toChars(kanjiId)));

                final TextView readingsText = (TextView)view.findViewById(R.id.backReadings);
                readingsText.setText("");

                final Yomi yomi = ReadingsManager.getInstance().yomiForKanji(kanjiId);
                if (yomi != null) {
                    StringBuilder sb = new StringBuilder();
                    for (String r : yomi.getReadings()) {
                        sb.append(r);
                        sb.append("\n");
                    }
                    if (yomi.getNanori() != null) {
                        sb.append("\n   Nanori:\n");
                        for (String n : yomi.getNanori()) {
                            sb.append(n);
                            sb.append("\n");
                        }
                    }
                    readingsText.setText(sb.toString());
                }

                final Button btnStudyPage = (Button)view.findViewById(R.id.button_study_page);
                btnStudyPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String kanji = new String(Character.toChars(kanjiId));
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://kanji.koohii.com/study/kanji/" + kanji)));
                    }
                });
            }

            return view;
        }

        @Override
        public void onResume() {
            super.onResume();

            if (needsRestore) {
                willAppear();
                needsRestore = false;
            }
        }

        public void willAppear() {
            final ImageView backImage = (ImageView)getView().findViewById(R.id.backImage);
            final ReviewActivity activity = (ReviewActivity)getActivity();
            backImage.setImageBitmap(activity.bitmap);
        }
    }

}
