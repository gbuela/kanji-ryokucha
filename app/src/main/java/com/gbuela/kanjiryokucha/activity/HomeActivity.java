package com.gbuela.kanjiryokucha.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gbuela.kanjiryokucha.GlobalState;
import com.gbuela.kanjiryokucha.R;
import com.gbuela.kanjiryokucha.database.KoohiiData;
import com.gbuela.kanjiryokucha.database.ReviewDbHelper;
import com.gbuela.kanjiryokucha.model.GlobalRecord;
import com.gbuela.kanjiryokucha.model.ReviewStatus;
import com.gbuela.kanjiryokucha.model.ReviewType;
import com.gbuela.kanjiryokucha.network.KoohiiTask;
import com.gbuela.kanjiryokucha.network.KoohiiTaskHandler;
import com.gbuela.kanjiryokucha.network.task.GetStatusTask;
import com.gbuela.kanjiryokucha.readings.ReadingsManager;
import com.gbuela.kanjiryokucha.utils.persistentcookies.PersistentCookieStore;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class HomeActivity extends KoohiiActivity implements View.OnClickListener, KoohiiActivity.KoohiiConfirm.OKHandler {
    private SharedPreferences preferences;
    private String currentApplicationVersion = "UNKNOWN";
    private String lastInstalledApplicationVersion = "UNKNOWN";

    static final int CONFIRMTAG_LOGOUT = 1;

    static final String STATE_NUMDUE = "STATE_NUMDUE";
    static final String STATE_NUMNEW = "STATE_NUMNEW";
    static final String STATE_NUMFAIL = "STATE_NUMFAIL";
    static final String STATE_USERNAME = "STATE_USERNAME";

    private static final String PREFERENCES_FILE = "prefs";

    private View buttonDue;
    private View buttonNew;
    private View buttonFailed;
    private TextView textDue;
    private TextView textNew;
    private TextView textFailed;
    private TextView textSessionDue;
    private TextView textSessionNew;
    private TextView textSessionFailed;
    private TextView textUsername;
    private Button buttonSite;
    private ProgressBar progressBar;
    private ReviewStatus reviewStatus;
    private KoohiiData data;

    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            // http://stackoverflow.com/questions/6337217/
            // doesn't launch the same way from lanucher and from slideme store
            finish();
            return;
        }

        Fabric.with(this, new Crashlytics());

        try {
            currentApplicationVersion = this.getPackageManager()
                                              .getPackageInfo(this.getPackageName(), 0)
                                              .versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            // fail silently
        }
        preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        setContentView(R.layout.activity_home);

        if (savedInstanceState == null) {
            initialize();

            reviewStatus = new ReviewStatus();
            username = "";
            textUsername.setText("");
        }
    }

    private void initialize() {
        buttonDue = this.findViewById(R.id.button_expired);
        buttonNew = this.findViewById(R.id.button_new);
        buttonFailed = this.findViewById(R.id.button_failed);
        textDue = (TextView)this.findViewById(R.id.button_expired_text);
        textNew = (TextView)this.findViewById(R.id.button_new_text);
        textFailed = (TextView)this.findViewById(R.id.button_failed_text);
        textSessionDue = (TextView)this.findViewById(R.id.button_expired_progress);
        textSessionNew = (TextView)this.findViewById(R.id.button_new_progress);
        textSessionFailed = (TextView)this.findViewById(R.id.button_failed_progress);
        buttonSite = (Button)this.findViewById(R.id.button_site);
        progressBar = (ProgressBar)this.findViewById(R.id.progressBar);

        textUsername = (TextView) findViewById(R.id.text_username);

        buttonDue.setOnClickListener(this);
        buttonNew.setOnClickListener(this);
        buttonFailed.setOnClickListener(this);
        buttonSite.setOnClickListener(this);

        textSessionDue.setVisibility(View.GONE);
        textSessionNew.setVisibility(View.GONE);
        textSessionFailed.setVisibility(View.GONE);

        ReadingsManager.getInstance().startLoadingReadings(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putInt(STATE_NUMDUE, reviewStatus.getNumberDueCards());
        outState.putInt(STATE_NUMNEW, reviewStatus.getNumberNewCards());
        outState.putInt(STATE_NUMFAIL, reviewStatus.getNumberFailedCards());
        outState.putString(STATE_USERNAME, username);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        initialize();

        reviewStatus = new ReviewStatus();
        reviewStatus.setNumberDueCards(savedInstanceState.getInt(STATE_NUMDUE));
        reviewStatus.setNumberNewCards(savedInstanceState.getInt(STATE_NUMNEW));
        reviewStatus.setNumberFailedCards(savedInstanceState.getInt(STATE_NUMFAIL));

        username = savedInstanceState.getString(STATE_USERNAME);
        textUsername.setText(username);
    }

    @Override
    protected void onResume() {
        super.onResume();

        lastInstalledApplicationVersion = preferences.getString("app.version","0.0");
        if (lastInstalledApplicationVersion == null || !lastInstalledApplicationVersion.equals(currentApplicationVersion)) {
            final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.hang_on_message), true);
             new AsyncTask <Void,Void,Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    new ReviewDbHelper(HomeActivity.this); //Open database to force upgrade
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("app.version", currentApplicationVersion);
                    editor.apply();
                    progressDialog.dismiss();
                    resumeDbReady();
                }
             }.execute();
        }
        else {
            resumeDbReady();
        }
    }

    private void resumeDbReady() {
        data = new KoohiiData(this);

        updateButtons();

        if(GlobalState.getInstance().isNeedsRefresh()) {
            getCurrentStatus();
        }
    }

    private void updateButtons() {

        final long numEntries = data.countEntries();
        final boolean openSession = (numEntries > 0);

        textSessionDue.setVisibility(View.GONE);
        textSessionNew.setVisibility(View.GONE);
        textSessionFailed.setVisibility(View.GONE);

        final GlobalRecord global = data.getGlobalRecord();
        final ReviewType sessionType = openSession? global.getReviewType() : ReviewType.NONE;

        updateButton(buttonDue,
                textDue,
                textSessionDue,
                ReviewType.DUE,
                getResources().getColor(R.color.koohiiOrange),
                getResources().getColor(R.color.koohiiOrangeDisabled),
                reviewStatus.getNumberDueCards(),
                sessionType);

        updateButton(buttonNew,
                textNew,
                textSessionNew,
                ReviewType.NEW,
                getResources().getColor(R.color.koohiiBlue),
                getResources().getColor(R.color.koohiiBlueDisabled),
                reviewStatus.getNumberNewCards(),
                sessionType);

        updateButton(buttonFailed,
                textFailed,
                textSessionFailed,
                ReviewType.FAILED,
                getResources().getColor(R.color.koohiiRed),
                getResources().getColor(R.color.koohiiRedDisabled),
                reviewStatus.getNumberFailedCards(),
                sessionType);
    }

    @Override
    protected void onLoggedIn() {
        final GlobalRecord global = data.getGlobalRecord();
        Crashlytics.setUserIdentifier(global.getUsername());
    }

    @Override
    public void onClick(View v) {
        if (v == buttonDue) {
            startSession(ReviewType.DUE, reviewStatus.getNumberDueCards());
        }
        else if (v == buttonNew) {
            startSession(ReviewType.NEW, reviewStatus.getNumberNewCards());
        }
        else if (v == buttonFailed) {
            startSession(ReviewType.FAILED, reviewStatus.getNumberDueCards());
        }
        else if (v == buttonSite) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://kanji.koohii.com/")));
        }
    }

    private void startSession(ReviewType type, int numCards) {
        final long numEntries = data.countEntries();
        final boolean newSession = (numEntries == 0);

        final Intent intent = SessionActivity.getSessionActivityIntent(this, newSession, type, numCards);
        startActivity(intent);
    }

    // TODO: not fetching info for now...
    /*
    private void getInfoAndStatus() {
        if (username == null || username == "") {
            new GetAccountInfoTask().execute(this, new KoohiiTaskHandler<AccountInfo>() {
                @Override
                public void handle(AccountInfo accountInfo) {
                    username = accountInfo.getUsername();
                    textUsername.setText(accountInfo.getUsername());
//                    buttonCustom.setEnabled(true);
                    getCurrentStatus();
                }
            });
        } else {
            getCurrentStatus();
        }
    }
    */

    private void getCurrentStatus() {
        final GetStatusTask task = new GetStatusTask();
        task.execute(this, new KoohiiTaskHandler<ReviewStatus>() {
            @Override
            public void handle(ReviewStatus status) {
                GlobalState.getInstance().setNeedsRefresh(false);
                reviewStatus = status;
                updateButtons();
            }
        });
    }

    private void updateButton(View button,
                              TextView buttonText,
                              TextView sessionText,
                              ReviewType reviewType,
                              int color,
                              int disabledColor,
                              int numCards,
                              ReviewType sessionReviewType)
    {
        buttonText.setText("" + numCards);

        if ((numCards > 0 && sessionReviewType == ReviewType.NONE) || sessionReviewType == reviewType) {
            button.setBackgroundColor(color);
            button.setEnabled(true);
        }
        else {
            button.setBackgroundColor(disabledColor);
            button.setEnabled(false);
        }

        sessionText.setVisibility((sessionReviewType == reviewType)? View.VISIBLE :View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_customreview:
                final Intent intentCustom = new Intent(this, CustomReviewActivity.class);
                startActivity(intentCustom);
                return true;
            case R.id.action_refreshstatus:
                getCurrentStatus();
                return true;
            case R.id.action_logout:
                showConfirmationDialog(CONFIRMTAG_LOGOUT, "Logout", "Are you sure?");
                return true;
            case R.id.action_about:
                final Intent intentAbout = new Intent(this, AboutActivity.class);
                startActivity(intentAbout);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void userConfirmed(int tag) {

        if (tag == CONFIRMTAG_LOGOUT) {
            // TODO: end server session & initialize home UI
            new PersistentCookieStore(this).removeAll();

            final GlobalRecord global = data.getGlobalRecord();
            global.setUsername("");
            global.setPassword("");
            data.saveGlobalRecord(global);
        }
    }

    @Override
    public void onTaskStart(KoohiiTask task) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskEnd(KoohiiTask task) {
        progressBar.setVisibility(View.INVISIBLE);
    }
}
