package com.gbuela.kanjiryokucha.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.gbuela.kanjiryokucha.R;
import com.gbuela.kanjiryokucha.database.KoohiiData;
import com.gbuela.kanjiryokucha.model.Card;

import java.util.List;

public class StudyActivity extends KoohiiActivity {

    private KoohiiData data;
    private List<Card> mCards;
    private ListView listView;
    private StudyListAdapter adapter;

    public static Intent getStudyActivityIntent(Context context) {
        Intent intent = new Intent(context, StudyActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);
    }

    @Override
    protected void onResume() {
        super.onResume();

        listView = (ListView)findViewById(R.id.listView);

        getActionBar().setTitle(getString(R.string.study_title));

        data = new KoohiiData(this);
        mCards = data.getStudyPending();

        final Button allReadyButton = (Button)findViewById(R.id.button_ready);
        allReadyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allReadyButton.setEnabled(false);
                data.deleteAllStudy();
                finish();
            }
        });

        adapter = new StudyListAdapter(this, mCards);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Card card = mCards.get(position);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://kanji.koohii.com/study/kanji/" + card.getFrameNum())));
            }
        });

        adapter.setButtonHandler(new ButtonHandler() {
            @Override
            public void handle(Card card) {
                data.deleteStudy(card);
                mCards.remove(card);
                adapter.notifyDataSetChanged();
                if (mCards.size() == 0) {
                    finish();
                }
            }
        });
    }
}
