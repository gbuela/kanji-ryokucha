package com.gbuela.kanjiryokucha.activity;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import com.gbuela.kanjiryokucha.R;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        final WebView wv = (WebView)findViewById(R.id.webView);
        wv.loadUrl("file:///android_asset/about.html");
        try {
            final PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            final TextView textVersion = (TextView)findViewById(R.id.text_version);
            textVersion.setText(getString(R.string.version_label) + pi.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        getActionBar().setTitle(getString(R.string.about_title));
    }

}
