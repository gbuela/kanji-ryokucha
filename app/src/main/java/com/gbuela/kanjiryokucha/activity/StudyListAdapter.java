package com.gbuela.kanjiryokucha.activity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gbuela.kanjiryokucha.R;
import com.gbuela.kanjiryokucha.model.Card;

import java.util.List;

public class StudyListAdapter extends ArrayAdapter<Card> {

    private Activity context;
    private List<Card> cards;

    public ButtonHandler getButtonHandler() {
        return buttonHandler;
    }

    public void setButtonHandler(ButtonHandler buttonHandler) {
        this.buttonHandler = buttonHandler;
    }

    private ButtonHandler buttonHandler;

    static class ViewHolder {
        public TextView text;
        public ImageView image;
    }


    public StudyListAdapter(Activity context, List<Card> cards) {
        super(context, R.layout.row_studyitem, cards);
        this.context = context;
        this.cards = cards;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.row_studyitem, null);
            // configure view holder
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.text_keyword);
            viewHolder.image = (ImageView) rowView
                    .findViewById(R.id.button_itemready);
            rowView.setTag(viewHolder);
        }
        // fill data
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        final Card card = cards.get(position);
        holder.text.setText(card.getKeyword());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonHandler != null) {
                    buttonHandler.handle(card);
                }
            }
        });
        return rowView;
    }
}

interface ButtonHandler {
    void handle(Card card);
}
