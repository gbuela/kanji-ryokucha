package com.gbuela.kanjiryokucha.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.gbuela.kanjiryokucha.database.GlobalContract.GlobalEntry;
import com.gbuela.kanjiryokucha.database.ReviewContract.ReviewEntry;
import com.gbuela.kanjiryokucha.database.StudyContract.StudyEntry;
import com.gbuela.kanjiryokucha.model.Card;
import com.gbuela.kanjiryokucha.model.CardAnswer;
import com.gbuela.kanjiryokucha.model.CardIdList;
import com.gbuela.kanjiryokucha.model.GlobalRecord;
import com.gbuela.kanjiryokucha.model.ReviewType;

import java.util.ArrayList;
import java.util.List;

public class KoohiiData {

    private Context context;

    public KoohiiData(Context context) {
        this.context = context;
    }

    private ContentValues valuesForCard(int cardId, CardAnswer answer) {
        final ContentValues values = new ContentValues();
        values.put(ReviewEntry.COLUMN_NAME_CARD_ID, cardId);
        values.put(ReviewEntry.COLUMN_NAME_ANSWER, answer.getCode());
        return values;
    }

    private ContentValues valuesForStudyCard(Card card) {
        final ContentValues values = new ContentValues();
        values.put(StudyEntry.COLUMN_NAME_CARD_ID, card.getKanjiId());
        values.put(StudyEntry.COLUMN_NAME_KEYWORD, card.getKeyword());
        values.put(StudyEntry.COLUMN_NAME_FRAMENUM, card.getFrameNum());
        values.put(StudyEntry.COLUMN_NAME_STROKECOUNT, card.getStrokeCount());
        return values;
    }

    public void update(Card card) {

        final ContentValues values = new ContentValues();
        values.put(ReviewEntry.COLUMN_NAME_ANSWER, card.getAnswer().getCode());
        values.put(ReviewEntry.COLUMN_NAME_TIMESTAMP, "CURRENT_TIMESTAMP");
        if (card.getKeyword() != null) {
            values.put(ReviewEntry.COLUMN_NAME_KEYWORD, card.getKeyword());
        }
        if (card.getFrameNum() > 0) {
            values.put(ReviewEntry.COLUMN_NAME_FRAMENUM, card.getFrameNum());
        }
        if (card.getStrokeCount() > 0) {
            values.put(ReviewEntry.COLUMN_NAME_STROKECOUNT, card.getStrokeCount());
        }

        final String selection = ReviewEntry.COLUMN_NAME_CARD_ID + " = ?";
        final String[] selectionArgs = { String.valueOf(card.getKanjiId()) };

        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {
                db.update(
                        ReviewEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
            }
        });
    }

    public void deleteAll() {
        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {
                KoohiiData.this.deleteAll(db);
            }
        });
    }

    public void deleteAllStudy() {
        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {
                db.delete(StudyEntry.TABLE_NAME, null, null);
            }
        });
    }

    private void deleteAll(SQLiteDatabase db) {
        db.delete(ReviewEntry.TABLE_NAME, null, null);
        db.delete(GlobalEntry.TABLE_NAME, null, null);
    }

    public void clearSession() {
        final ContentValues values = new ContentValues();
        values.put(GlobalEntry.COLUMN_NAME_REVIEW_TYPE, ReviewType.NONE.getCode());

        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {
                db.delete(ReviewEntry.TABLE_NAME, null, null);
                db.update(GlobalEntry.TABLE_NAME, values, null, null);
            }
        });
    }

    public long countAnsweredEntries() {
        final Long count = (Long)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {
                final String selection = ReviewEntry.COLUMN_NAME_ANSWER + " <> ?";
                return DatabaseUtils.queryNumEntries(db,
                        ReviewEntry.TABLE_NAME,
                        selection,
                        new String[] { String.valueOf(CardAnswer.UNANSWERED.getCode())});
            }
        });
        return count;
    }

    public long countStudyPendingEntries() {
        final Long count = (Long)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {
                return DatabaseUtils.queryNumEntries(db, StudyEntry.TABLE_NAME);
            }
        });
        return count;
    }

    public long countEntries() {
        final Long count = (Long)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {
                return DatabaseUtils.queryNumEntries(db, ReviewEntry.TABLE_NAME);
            }
        });
        return count;
    }

    public void initializeReview(final CardIdList cardIdList, final ReviewType reviewType) {

        final GlobalRecord global = getGlobalRecord();

        global.setReviewType(reviewType);

        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {

                KoohiiData.this.deleteAll(db);

                for (Integer cardId : cardIdList.getCardIds()) {
                    db.insert(ReviewEntry.TABLE_NAME,
                            null,
                            KoohiiData.this.valuesForCard(cardId, CardAnswer.UNANSWERED));
                }

                KoohiiData.this.saveGlobalRecord(db, global);

                if (reviewType == ReviewType.FAILED) {
                    db.execSQL("DELETE FROM " + StudyEntry.TABLE_NAME +
                            " WHERE " + StudyEntry.COLUMN_NAME_CARD_ID +
                            " NOT IN (" + TextUtils.join(",", cardIdList.getCardIds()) + ")");
                }
            }
        });
    }

    public void insertStudy(final Card card) {
        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {
                db.insert(StudyEntry.TABLE_NAME,
                        null,
                        KoohiiData.this.valuesForStudyCard(card));
            }
        });
    }

    public void deleteStudy(final Card card) {
        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {
                db.delete(StudyEntry.TABLE_NAME,
                        StudyEntry.COLUMN_NAME_CARD_ID + " = ?",
                        new String[]{ "" + card.getKanjiId() });
            }
        });
    }

    public GlobalRecord getGlobalRecord() {

        return (GlobalRecord)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {
                final String[] projection = {
                        GlobalEntry.COLUMN_NAME_REVIEW_TYPE,
                        GlobalEntry.COLUMN_NAME_USERNAME,
                        GlobalEntry.COLUMN_NAME_PASSWORD
                };

                final Cursor cursor = db.query(
                        GlobalEntry.TABLE_NAME,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null
                );

                int reviewType = ReviewType.NONE.getCode();
                String username = "";
                String password = "";
                if (cursor.moveToFirst()) {
                    reviewType = cursor.getInt(cursor.getColumnIndex(GlobalEntry.COLUMN_NAME_REVIEW_TYPE));
                    username = cursor.getString(cursor.getColumnIndex(GlobalEntry.COLUMN_NAME_USERNAME));
                    password = cursor.getString(cursor.getColumnIndex(GlobalEntry.COLUMN_NAME_PASSWORD));
                }
                cursor.close();

                final GlobalRecord record = new GlobalRecord();
                record.setReviewType(ReviewType.typeFromInt(reviewType));
                record.setUsername(username);
                record.setPassword(password);
                return record;
            }
        });
    }

    public CardIdList getStudyCardIds() {
        final CardIdList cardIdList = (CardIdList)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {

                final ArrayList<Integer> ids = new ArrayList<>();

                final Cursor cursor = db.query(
                        StudyEntry.TABLE_NAME,
                        new String[] { StudyEntry.COLUMN_NAME_CARD_ID },
                        null,
                        null,
                        null,
                        null,
                        null
                );

                if (cursor.moveToFirst()) {
                    do {
                        final int anId = cursor.getInt(cursor.getColumnIndex(StudyEntry.COLUMN_NAME_CARD_ID));
                        ids.add(anId);
                    } while (cursor.moveToNext());
                }
                cursor.close();

                final CardIdList cardIdList = new CardIdList();
                cardIdList.setCardIds(ids);
                return cardIdList;
            }
        });

        return cardIdList;
    }

    public CardIdList getUnanswaredCardIds() {

        final CardIdList cardIdList = (CardIdList)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {

                final ArrayList<Integer> ids = new ArrayList<>();

                final Cursor cursor = db.query(
                        ReviewEntry.TABLE_NAME,
                        new String[] { ReviewEntry.COLUMN_NAME_CARD_ID },
                        ReviewEntry.COLUMN_NAME_ANSWER + " = ?",
                        new String[] { "" + CardAnswer.UNANSWERED.getCode() },
                        null,
                        null,
                        null
                );

                if (cursor.moveToFirst()) {
                    do {
                        final int anId = cursor.getInt(cursor.getColumnIndex(ReviewEntry.COLUMN_NAME_CARD_ID));
                        ids.add(anId);
                    } while (cursor.moveToNext());
                }
                cursor.close();

                final CardIdList cardIdList = new CardIdList();
                cardIdList.setCardIds(ids);
                return cardIdList;            }
        });

        return cardIdList;
    }

    @SuppressWarnings("unchecked")
    public List<Card> getStudyPending() {

        final ArrayList<Card> cards = (ArrayList<Card>)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {
                final ArrayList<Card> cards = new ArrayList<>();

                final Cursor cursor = db.query(
                        StudyEntry.TABLE_NAME,
                        new String[] {
                                StudyEntry.COLUMN_NAME_CARD_ID,
                                StudyEntry.COLUMN_NAME_KEYWORD,
                                StudyEntry.COLUMN_NAME_TIMESTAMP,
                                StudyEntry.COLUMN_NAME_FRAMENUM,
                                StudyEntry.COLUMN_NAME_STROKECOUNT },
                        null,
                        null,
                        null,
                        null,
                        null
                );

                if (cursor.moveToFirst()) {
                    do {
                        final Card card = new Card();
                        card.setKanjiId(cursor.getInt(cursor.getColumnIndex(StudyEntry.COLUMN_NAME_CARD_ID)));
                        card.setKeyword(cursor.getString(cursor.getColumnIndex(StudyEntry.COLUMN_NAME_KEYWORD)));
                        card.setmAnswerTimestamp(cursor.getLong(cursor.getColumnIndex(StudyEntry.COLUMN_NAME_TIMESTAMP)));
                        card.setFrameNum(cursor.getInt(cursor.getColumnIndex(StudyEntry.COLUMN_NAME_FRAMENUM)));
                        card.setStrokeCount(cursor.getInt(cursor.getColumnIndex(StudyEntry.COLUMN_NAME_STROKECOUNT)));
                        cards.add(card);
                    } while (cursor.moveToNext());
                }
                cursor.close();

                return cards;
            }
        });

        return cards;
    }

    @SuppressWarnings("unchecked")
    public List<Card> getAnswers(final int limit) {

        final ArrayList<Card> cardAnswers = (ArrayList<Card>)ReviewDbHelper.doReadOperation(this.context, new ReadOperation() {
            @Override
            public Object execute(SQLiteDatabase db) {
                final ArrayList<Card> cardAnswers = new ArrayList<>();

                final Cursor cursor = db.query(
                        ReviewEntry.TABLE_NAME,
                        new String[] {
                                ReviewEntry.COLUMN_NAME_CARD_ID,
                                ReviewEntry.COLUMN_NAME_ANSWER,
                                ReviewEntry.COLUMN_NAME_TIMESTAMP,
                                ReviewEntry.COLUMN_NAME_KEYWORD,
                                ReviewEntry.COLUMN_NAME_FRAMENUM,
                                ReviewEntry.COLUMN_NAME_STROKECOUNT
                        },
                        ReviewEntry.COLUMN_NAME_ANSWER + " != ?",
                        new String[] { "" + CardAnswer.UNANSWERED.getCode() },
                        null,
                        null,
                        null
                );

                if (cursor.moveToFirst()) {
                    int i = 0;
                    do {
                        final Card card = new Card();
                        card.setKanjiId(cursor.getInt(cursor.getColumnIndex(ReviewEntry.COLUMN_NAME_CARD_ID)));
                        card.setAnswer(CardAnswer.cardAnswerFromInt(cursor.getInt(cursor.getColumnIndex(ReviewEntry.COLUMN_NAME_ANSWER))));
                        card.setmAnswerTimestamp(cursor.getLong(cursor.getColumnIndex(ReviewEntry.COLUMN_NAME_TIMESTAMP)));
                        card.setKeyword(cursor.getString(cursor.getColumnIndex(ReviewEntry.COLUMN_NAME_KEYWORD)));
                        card.setFrameNum(cursor.getInt(cursor.getColumnIndex(ReviewEntry.COLUMN_NAME_FRAMENUM)));
                        card.setStrokeCount(cursor.getInt(cursor.getColumnIndex(ReviewEntry.COLUMN_NAME_STROKECOUNT)));
                        cardAnswers.add(card);
                        i++;
                    } while (i < limit && cursor.moveToNext());
                }
                cursor.close();

                return cardAnswers;
            }
        });

        return cardAnswers;
    }

    public void deleteSubmittedEntries(final List<Integer> cardsIds) {
        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {

                final String[] ids = new String[cardsIds.size()];
                int i = 0;
                for(int id: cardsIds) { ids[i++] = String.valueOf(id); }
                final String args = TextUtils.join(", ", ids);

                db.execSQL(String.format("DELETE FROM " + ReviewEntry.TABLE_NAME + " WHERE " +
                        ReviewEntry.COLUMN_NAME_CARD_ID + " IN (%s);", args));
            }
        });
    }

    private void saveGlobalRecord(SQLiteDatabase db, GlobalRecord global) {

        db.delete(GlobalEntry.TABLE_NAME, null, null);
        final ContentValues values = new ContentValues();
        values.put(GlobalEntry.COLUMN_NAME_REVIEW_TYPE, global.getReviewType().getCode());
        values.put(GlobalEntry.COLUMN_NAME_USERNAME, global.getUsername());
        values.put(GlobalEntry.COLUMN_NAME_PASSWORD, global.getPassword());

        db.insert(GlobalEntry.TABLE_NAME,
                null,
                values);
    }

    public void saveGlobalRecord(final GlobalRecord global) {
        ReviewDbHelper.doWriteOperation(this.context, new WriteOperation() {
            @Override
            public void execute(SQLiteDatabase db) {
                saveGlobalRecord(db, global);
            }
        });
    }

}
