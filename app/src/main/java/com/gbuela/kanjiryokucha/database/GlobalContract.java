package com.gbuela.kanjiryokucha.database;


import android.provider.BaseColumns;

public final class GlobalContract {

    private GlobalContract() {}

    public static abstract class GlobalEntry implements BaseColumns {
        public static final String TABLE_NAME = "globalentry";
        public static final String COLUMN_NAME_REVIEW_TYPE = "reviewtype";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";

        private static final String INTEGER_TYPE = " INTEGER";
        private static final String TEXT_TYPE = " TEXT";

        private static final String COMMA_SEP = ",";
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + GlobalEntry.TABLE_NAME + " (" +
                        GlobalEntry._ID + " INTEGER PRIMARY KEY," +
                        GlobalEntry.COLUMN_NAME_REVIEW_TYPE + INTEGER_TYPE + COMMA_SEP +
                        GlobalEntry.COLUMN_NAME_USERNAME + TEXT_TYPE + COMMA_SEP +
                        GlobalEntry.COLUMN_NAME_PASSWORD + TEXT_TYPE +
                        " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + GlobalEntry.TABLE_NAME;
    }

}
