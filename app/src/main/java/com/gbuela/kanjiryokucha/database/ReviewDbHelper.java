package com.gbuela.kanjiryokucha.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gbuela.kanjiryokucha.database.ReviewContract.ReviewEntry;
import com.gbuela.kanjiryokucha.database.GlobalContract.GlobalEntry;
import com.gbuela.kanjiryokucha.database.StudyContract.StudyEntry;

public class ReviewDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "KoohiiReviews.db";

    private static final Patch[] patches = new Patch[] {
        new Patch() { // creation -> 1
            @Override
            public void apply(SQLiteDatabase db) {
                db.execSQL(ReviewEntry.SQL_CREATE_ENTRIES);
                db.execSQL(GlobalEntry.SQL_CREATE_ENTRIES);
            }
        },
         new Patch() { // 1 -> 2
            @Override
            public void apply(SQLiteDatabase db) {
                db.execSQL(ReviewEntry.SQL_MIGRATE_1_2_KEYWORD);
                db.execSQL(ReviewEntry.SQL_MIGRATE_1_2_FRAMENUM);
                db.execSQL(ReviewEntry.SQL_MIGRATE_1_2_STROKECOUNT);
                db.execSQL(StudyEntry.SQL_CREATE_ENTRIES);
            }
        }
    };

    public ReviewDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (int i = 0; i < patches.length; i++) {
            patches[i].apply(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int i = oldVersion; i < newVersion; i++) {
            patches[i].apply(db);
        }
    }

    public static Object doReadOperation(Context context, ReadOperation operation) {
        final ReviewDbHelper reviewDbHelper = new ReviewDbHelper(context);
        final SQLiteDatabase db = reviewDbHelper.getReadableDatabase();
        Object result = null;
        try {
            result = operation.execute(db);
        }
        finally {
            db.close();
        }
        return result;
    }
    public static void doWriteOperation(Context context, WriteOperation operation) {
        final ReviewDbHelper reviewDbHelper = new ReviewDbHelper(context);
        final SQLiteDatabase db = reviewDbHelper.getWritableDatabase();
        try {
            operation.execute(db);
        }
        finally {
            db.close();
        }
    }

    private static abstract class Patch {
        public abstract void apply(SQLiteDatabase db);
    }
}
