package com.gbuela.kanjiryokucha.database;

import android.database.sqlite.SQLiteDatabase;

public interface ReadOperation {
    Object execute(SQLiteDatabase db);
}

