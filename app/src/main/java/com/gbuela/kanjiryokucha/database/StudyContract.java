package com.gbuela.kanjiryokucha.database;

import android.provider.BaseColumns;

public final class StudyContract {

    private StudyContract() {}

    public static abstract class StudyEntry implements BaseColumns {
        public static final String TABLE_NAME = "studyentry";
        public static final String COLUMN_NAME_CARD_ID = "cardid";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_KEYWORD = "keyword";
        public static final String COLUMN_NAME_FRAMENUM = "framenum";
        public static final String COLUMN_NAME_STROKECOUNT = "strokes";

        private static final String TEXT_TYPE = " TEXT";
        private static final String INTEGER_TYPE = " INTEGER";
        private static final String COMMA_SEP = ",";
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + StudyEntry.TABLE_NAME + " (" +
                        StudyEntry._ID + " INTEGER PRIMARY KEY," +
                        StudyEntry.COLUMN_NAME_CARD_ID + INTEGER_TYPE + COMMA_SEP +
                        StudyEntry.COLUMN_NAME_KEYWORD + TEXT_TYPE + COMMA_SEP +
                        StudyEntry.COLUMN_NAME_FRAMENUM + INTEGER_TYPE + " DEFAULT 0" + COMMA_SEP +
                        StudyEntry.COLUMN_NAME_STROKECOUNT + INTEGER_TYPE + " DEFAULT 0" + COMMA_SEP +
                        StudyEntry.COLUMN_NAME_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" +
                        " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + StudyEntry.TABLE_NAME;
    }
}
