package com.gbuela.kanjiryokucha.model;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountInfo {

    private String username;
    private int flashcardCount;
    private int totalReviews;

    public static AccountInfo fromJson(JSONObject jo) {
        AccountInfo ai = new AccountInfo();

        try {
            ai.setUsername(jo.getString("username"));

            JSONObject srs = jo.getJSONObject("srs_info");

            ai.setFlashcardCount(srs.getInt("flashcard_count"));
            ai.setTotalReviews(srs.getInt("total_reviews"));
        } catch (JSONException e) {
            return null;
        }

        return ai;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getFlashcardCount() {
        return flashcardCount;
    }

    public void setFlashcardCount(int flashcardCount) {
        this.flashcardCount = flashcardCount;
    }

    public int getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(int totalReviews) {
        this.totalReviews = totalReviews;
    }
}
