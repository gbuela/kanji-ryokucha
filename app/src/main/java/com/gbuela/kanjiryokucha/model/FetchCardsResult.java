package com.gbuela.kanjiryokucha.model;

import java.util.ArrayList;

public class FetchCardsResult {

    private int startIndex;
    private ArrayList<Card> cards;

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }
}
