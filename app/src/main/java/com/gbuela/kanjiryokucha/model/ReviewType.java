package com.gbuela.kanjiryokucha.model;

public enum ReviewType {
    NONE(0),
    DUE(1),
    NEW(2),
    FAILED(3),
    FREE_RANGE(4);

    protected int code;

    ReviewType(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static ReviewType typeFromInt(int code) {
        switch (code) {
            case 0: return NONE;
            case 1: return DUE;
            case 2: return NEW;
            case 3: return FAILED;
            default: return FREE_RANGE;
        }
    }
}
