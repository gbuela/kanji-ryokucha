package com.gbuela.kanjiryokucha.model;

import java.util.List;

public class Yomi {

    private int kanjiId;
    private List<String> readings;
    private List<String> nanori;

    public int getKanjiId() {
        return kanjiId;
    }

    public void setKanjiId(int kanjiId) {
        this.kanjiId = kanjiId;
    }

    public List<String> getReadings() {
        return readings;
    }

    public void setReadings(List<String> readings) {
        this.readings = readings;
    }

    public List<String> getNanori() {
        return nanori;
    }

    public void setNanori(List<String> nanori) {
        this.nanori = nanori;
    }
}
