package com.gbuela.kanjiryokucha.network.task;

import com.gbuela.kanjiryokucha.model.CardIdList;
import com.gbuela.kanjiryokucha.network.KoohiiTask;

import org.json.JSONObject;

import java.util.HashMap;

public class ReviewRangeTask extends KoohiiTask<CardIdList> {

    private int from;
    private int to;
    private boolean shuffle;

    public ReviewRangeTask(int from, int to, boolean shuffle) {
        super();
        this.from = from;
        this.to = to;
        this.shuffle = shuffle;
    }

    @Override
    public String getApiMethod() {
        return "review/start";
    }

    @Override
    protected CardIdList resultFromJson(JSONObject json) {
        return CardIdList.fromJson(json);
    }

    @Override
    protected HashMap<String, String> buildQuerystringParams() {
        final HashMap<String, String> qs = new HashMap<>();

        qs.put("mode", "free");
        qs.put("from", "" + this.from);
        qs.put("to", "" + this.to);
        qs.put("shuffle", this.shuffle ? "1" : "0");

        return qs;
    }

    @Override
    protected KoohiiTask createTaskCopy() {
        return new ReviewRangeTask(this.from, this.to, this.shuffle);
    }
}

