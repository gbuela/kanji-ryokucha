package com.gbuela.kanjiryokucha.network;

public enum RequestMethod {
    GET,
    POST
}
