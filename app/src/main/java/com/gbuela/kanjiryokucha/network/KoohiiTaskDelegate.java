package com.gbuela.kanjiryokucha.network;

public interface KoohiiTaskDelegate {
    void onNoActiveSession(KoohiiTask task);
    void onTaskFailure(KoohiiTask task, String message);
    void onTaskStart(KoohiiTask task);
    void onTaskEnd(KoohiiTask task);
}
