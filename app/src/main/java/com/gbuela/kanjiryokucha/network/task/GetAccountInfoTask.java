package com.gbuela.kanjiryokucha.network.task;

import com.gbuela.kanjiryokucha.model.AccountInfo;
import com.gbuela.kanjiryokucha.network.KoohiiTask;

import org.json.JSONObject;

public class GetAccountInfoTask extends KoohiiTask<AccountInfo> {
    @Override
    public String getApiMethod() {
        return "account/info";
    }

    @Override
    protected AccountInfo resultFromJson(JSONObject json) {
        return AccountInfo.fromJson(json);
    }

    @Override
    protected KoohiiTask createTaskCopy() {
        return new GetAccountInfoTask();
    }
}
