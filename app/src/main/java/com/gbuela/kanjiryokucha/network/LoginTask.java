package com.gbuela.kanjiryokucha.network;

import android.os.AsyncTask;
//import android.util.Log;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class LoginTask extends AsyncTask<String,Void,String> {
    private static final String TAG = LoginTask.class.getSimpleName();
    private static final String TASKURL = KoohiiTask.HOST +  "/login";

    private LoginDelegate delegate;

    public LoginTask(LoginDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(String... urls) {

        String aString="";
        try {
            URL aURL = new URL(TASKURL);

            HashMap<String, String> postDataParams = new HashMap<String, String>();
            postDataParams.put("username", urls[0]);
            postDataParams.put("password", urls[1]);
            postDataParams.put("referer", "@homepage");
            postDataParams.put("commit", "Login");

            final HttpURLConnection conn = (HttpURLConnection) aURL.openConnection();

            conn.setRequestProperty("Host", KoohiiTask.DOMAIN);
            conn.setRequestProperty("Origin", KoohiiTask.HOST);
            conn.setRequestProperty("Referer", TASKURL);

            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(PostEncoder.getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                if (conn.getHeaderFields() == null){
                    //Log.v(TAG, "Header null");
                }else{
                    for (String cookie: conn.getHeaderFields().get("Location")) {
                        if(cookie.startsWith(KoohiiTask.HOST)) {
                            aString = "OK";
                            break;
                        }
                    }
                }
            }

        } catch (Exception e) {
            //Log.d(TAG, e.toString());
        }
        return aString;
    }

    @Override
    protected void onPostExecute(String result) {
        //Log.d(TAG, result);

        this.delegate.onLogin(result.equals("OK"));
    }
}

