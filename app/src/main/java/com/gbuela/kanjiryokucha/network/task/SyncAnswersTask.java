package com.gbuela.kanjiryokucha.network.task;

import com.gbuela.kanjiryokucha.model.Card;
import com.gbuela.kanjiryokucha.network.ContentType;
import com.gbuela.kanjiryokucha.network.KoohiiTask;
import com.gbuela.kanjiryokucha.network.RequestMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SyncAnswersTask extends KoohiiTask<List<Integer>> {

    private List<Card> cardAnswers;

    public SyncAnswersTask(List<Card> cardAnswers) {
        super();
        this.cardAnswers = cardAnswers;
    }

    @Override
    public String getApiMethod() {
        return "review/sync";
    }

    @Override
    public RequestMethod getRequestMethod() {
        return RequestMethod.POST;
    }

    @Override
    protected ContentType getContentType() {
        return ContentType.JSON;
    }

    @Override
    protected JSONObject buildJsonObject() {
        final JSONArray answers = new JSONArray();
        try {
            for(Card card : cardAnswers) {
                answers.put(jsonCardAnswer(card));
            }
            final JSONObject rootObject = new JSONObject();
            rootObject.put("time", 0);
            rootObject.put("sync", answers);
            return rootObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected KoohiiTask createTaskCopy() {
        return new SyncAnswersTask(this.cardAnswers);
    }

    @Override
    protected List<Integer> resultFromJson(JSONObject json) {
        try {
            final JSONArray jsonIds = json.getJSONArray("put");
            if (jsonIds != null) {
                final ArrayList<Integer> ids = new ArrayList<>();
                for (int i = 0; i < jsonIds.length(); i++) {
                    ids.add(jsonIds.getInt(i));
                }

                if (json.has("ignored")) {
                    final JSONArray ignoredIds = json.getJSONArray("ignored");
                    if (ignoredIds != null) {
                        for (int i = 0; i < ignoredIds.length(); i++) {
                            ids.add(ignoredIds.getInt(i));
                        }
                    }
                }

                return ids;
            }
            else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject jsonCardAnswer(Card card) throws JSONException {
        final JSONObject o = new JSONObject();
        o.put("id", card.getKanjiId());
        final String stringAnswer = card.getAnswer().getStringCode();
        if (stringAnswer == null) {
            o.put("r", card.getAnswer().getCode());
        }
        else {
            o.put("r", stringAnswer);
        }
        return o;
    }
}
