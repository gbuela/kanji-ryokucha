package com.gbuela.kanjiryokucha.network;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

//import android.util.Log;

public abstract class KoohiiTask<Result> extends AsyncTask<Object, Void, String> implements Cloneable {
    public static final String DOMAIN = "kanji.koohii.com"; // production
  //  public static final String DOMAIN = "staging.koohii.com"; // staging

    public static final String HOST = "https://" + DOMAIN;

    private static final String TAG = KoohiiTask.class.getSimpleName();
    private static final int MIN_MILLISECONDS_BETWEEN_REQUESTS = 1200;
    private static final String KOOHII_ENDPOINT = HOST + "/api/v1/";

    private static Date dateLastResponse;

    private KoohiiTaskHandler<Result> handler;
    private KoohiiTaskDelegate delegate;

    public RequestMethod getRequestMethod() {
        return RequestMethod.GET;
    }

    public abstract String getApiMethod();

    public String getURL() {
        return null;
    }

    protected abstract KoohiiTask createTaskCopy();

    public void execute(KoohiiTaskDelegate delegate, KoohiiTaskHandler<Result> handler) {
        this.delegate = delegate;
        this.handler = handler;

        if (dateLastResponse == null) { // first task, can run right now
            execute();
        }
        else {
            final Date now = new Date();
            final long elapsed = now.getTime() - dateLastResponse.getTime();
            if (elapsed >= MIN_MILLISECONDS_BETWEEN_REQUESTS) { // can run task right now
                execute();
            } else { // defer task until min required time has elapsed since last task
                final long waitTime = MIN_MILLISECONDS_BETWEEN_REQUESTS - elapsed;
                //Log.d(TAG, "Deferring task for " + waitTime + " millis");
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        execute();
                    }
                }, waitTime);
            }
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        runDelegateUIHook(true);
    }

    private void runDelegateUIHook(final boolean taskStart) {
        if (delegate != null) {
            final Handler mainHandler = new Handler(Looper.getMainLooper());

            final Runnable mainRunnable = new Runnable() {
                @Override
                public void run() {
                    if (taskStart) {
                        delegate.onTaskStart(KoohiiTask.this);
                    }
                    else {
                        delegate.onTaskEnd(KoohiiTask.this);
                    }
                }
            };
            mainHandler.post(mainRunnable);
        }
    }

    protected HashMap<String,String> buildQuerystringParams() {
        return null;
    }

    protected HashMap<String,String> buildPostParams() {
        return null;
    }

    protected JSONObject buildJsonObject() {
        return null;
    }

    protected boolean expectJsonResponse() { return true; }

    protected boolean expectStatInResponse() { return true; }

    protected ContentType getContentType() { return ContentType.FORM; }

    @Override
    public Object clone() throws CloneNotSupportedException {
        final KoohiiTask newTask = this.createTaskCopy();
        newTask.handler = this.handler;
        newTask.delegate = this.delegate;
        return newTask;
    }

    @Override
    protected String doInBackground(Object... params) {
        dateLastResponse = new Date();
        String aString="";
        try {

            final String url = getURL();

            final StringBuilder sb = new StringBuilder();
            if (url == null) {
                sb.append(KOOHII_ENDPOINT);
                sb.append(getApiMethod());
            }
            else {
                sb.append(url);
            }

            sb.append("?");
            final HashMap<String,String> qsParams = buildQuerystringParams();
            if (qsParams != null) {
                sb.append(PostEncoder.getPostDataString(qsParams));
                sb.append("&");
            }

            final URL aURL = new URL(sb.toString());

            final HttpsURLConnection conn = (HttpsURLConnection) aURL.openConnection();

            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);

            switch (this.getRequestMethod())
            {
                case GET:
                    conn.setRequestMethod("GET");
                    break;
                case POST:
                    conn.setRequestMethod("POST");
                    break;
            }

            switch (getContentType()) {
                case FORM:
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    break;
                case JSON:
                    conn.setRequestProperty("Content-Type", "application/json");
                    break;
            }
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            if (getContentType() == ContentType.FORM) {
                final HashMap<String, String> postParams = buildPostParams();
                if (postParams != null) {
                    final OutputStream os = conn.getOutputStream();
                    final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    final String encoded = PostEncoder.getPostDataString(postParams);
                    writer.write(encoded);
                    writer.flush();
                    writer.close();
                    os.close();
                }
            }
            if (getContentType() == ContentType.JSON) {
                final JSONObject json = buildJsonObject();
                final OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
                out.write(json.toString());
                out.close();
            }

            final int responseCode = conn.getResponseCode();

            aString = readInputStreamToString(conn);

        } catch (final Exception e) {
            //Log.d(TAG, e.toString());
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    handleFailure(e.getMessage());
                }
            });
        }

        return aString;
    }

    @Override
    protected void onPostExecute(String result) {
        //Log.d(TAG, "Got response: " + result);

        if (!expectJsonResponse()) {
            handleSuccess(null);
            return;
        }

        JSONObject json = null;
        try {
            if (result != null) {
                json = new JSONObject(result);

                if (expectStatInResponse()) {

                    if (json.has("stat")) {
                        String stat = json.getString("stat");

                        if (stat.equals("fail")) {

                            if (json.has("code")) {
                                int code = json.getInt("code");

                                if (code == 96) {
                                    if (this.delegate != null) {
                                        this.delegate.onNoActiveSession(this);
                                        runDelegateUIHook(false);
                                    }
                                    return;
                                }
                            }

                            String message = "unknown failure";
                            if (json.has("message")) {
                                message = json.getString("message");
                            }

                            handleFailure(message);
                            return;
                        } else if (stat.equals("ok")) {
                            handleSuccess(json);
                        } else {
                            handleFailure("(kkm) Unrecognized status");
                        }
                    } else {
                        handleFailure("(kkm) status not found");
                    }
                }
                else {
                    handleSuccess(json);
                }
            }
            else {
                handleFailure("(kkm) null response");
            }

        } catch (JSONException e) {
            //Log.w(TAG, "(kkm) Could not parse response as JSON");
            handleFailure(e.getMessage());
        }
    }

    protected void handleFailure(String message) {
        if (this.delegate != null) {
            this.delegate.onTaskFailure(this, message);
        }
        runDelegateUIHook(false);
    }

    protected void handleSuccess(JSONObject result) {
        runDelegateUIHook(false);
        if (this.handler != null) {
            this.handler.handle(resultFromJson(result));
        }
    }

    protected abstract Result resultFromJson(JSONObject json);


    private String readInputStreamToString(HttpURLConnection connection) {
        String result = null;
        final StringBuffer sb = new StringBuffer();
        InputStream is = null;

        try {
            is = new BufferedInputStream(connection.getInputStream());
            final BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        }
        catch (Exception e) {
            //Log.i(TAG, "Error reading InputStream");
            result = null;
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    //Log.i(TAG, "Error closing InputStream");
                }
            }
        }

        return result;
    }
}

