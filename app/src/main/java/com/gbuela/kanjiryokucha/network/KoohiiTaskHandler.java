package com.gbuela.kanjiryokucha.network;

public interface KoohiiTaskHandler<T> {
     void handle(T result);
}
