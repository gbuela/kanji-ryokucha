package com.gbuela.kanjiryokucha.utils;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.zip.Inflater;

/*

Lots of error conditions here but they should be rare. By design we're just present a general failure
if anything gets out of the happy path.

 */

public class KanjiVGHelper {

    public static final int BUFFER_SIZE = 3000;
    public static final String KANJIVG_FILENAME = "kanjivg.dat";
    public static final String TEMPLATE_FILENAME = "strokeorder.html";
    public static final String PATHS_PLACEHOLDER = "$PATHS$";
    public static final String HTML_FILENAME = "temp_strokeorder.html";

    private Context context;

    public KanjiVGHelper(Context context) {
        this.context = context;
    }

    /**
     * Does all the work finding the kanji path data in the kanjivg binary file, building HTML
     * based on the template and the paths, saving the file and returning the URL to this file.
     *
     * @param kanjiId the kanji ID
     * @return URL to the file generated or null if any of the steps has failed
     */
    public String createHtmlFile(int kanjiId) {
        final String paths = readKanjiPaths(kanjiId);
        if (paths != null) {
            final String html = combineTemplateAndPaths(paths);
            if (html != null) {
                return writeHtmlFile(html);
            }
        }
        return null;
    }

    private String readKanjiPaths(int kanjiId) {
        InputStream is = null;
        String paths = null;

        try {
            is = this.context.getAssets().open(KANJIVG_FILENAME);

            final BufferedInputStream bis = new BufferedInputStream(is);
            final DataInputStream dataIn = new DataInputStream(bis);
            int id = dataIn.readInt();
            while (id != kanjiId && id != -1) {
                final int len = dataIn.readInt();
                long skipped = dataIn.skip(len);
                if(skipped < len) { // this, well, can happen... and the workaround seems to work
                    final int newLen = (int) (len - skipped);
                    byte[] buf = new byte[newLen];
                    dataIn.readFully(buf);
                }
                id = dataIn.readInt();
            }
            if (id == kanjiId) {
                final int len = dataIn.readInt();
                final byte[] data = new byte[len];
                dataIn.read(data, 0, len);
                final Inflater decompresser = new Inflater();
                decompresser.setInput(data, 0, len);
                final byte[] result = new byte[BUFFER_SIZE];
                final int resultLength = decompresser.inflate(result);
                decompresser.end();

                if (resultLength > 0) {
                    paths = new String(result, 0, resultLength, "UTF-8");
                }

            }
        } catch (Exception e) {
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }
        return paths;
    }

    private String combineTemplateAndPaths(String paths) {

        InputStream htmlIs = null;
        final StringBuffer sb = new StringBuffer();
        try {
            htmlIs = context.getAssets().open(TEMPLATE_FILENAME);

            final InputStreamReader htmlIsr = new InputStreamReader(htmlIs);
            final BufferedReader htmlBr = new BufferedReader(htmlIsr);
            String line = htmlBr.readLine();
            while (line != null) {
                if (line.equals(PATHS_PLACEHOLDER)) {
                    sb.append(paths);
                }
                else {
                    sb.append(line);
                }
                sb.append("\n");
                line = htmlBr.readLine();
            }
        } catch (IOException e) {
            return null;
        } finally {
            if (htmlIs != null) {
                try {
                    htmlIs.close();
                } catch (IOException e) {
                }
            }
        }
        return sb.toString();
    }

    private String writeHtmlFile(String html) {
        OutputStreamWriter out = null;
        try {
            out = new OutputStreamWriter(context.openFileOutput(HTML_FILENAME,0));
            out.write(html);
        } catch (Exception e) {
            return null;
        }
        finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
        return "file://" + context.getFilesDir() + "/" + HTML_FILENAME;
    }
}
