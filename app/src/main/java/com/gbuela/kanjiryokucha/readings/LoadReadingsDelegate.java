package com.gbuela.kanjiryokucha.readings;

import com.gbuela.kanjiryokucha.model.Yomi;

import java.util.HashMap;

public interface LoadReadingsDelegate {
    void onLoadedReadings(HashMap<Integer, Yomi> allReadings);
}
