package com.gbuela.kanjiryokucha.readings;

import android.content.Context;

import com.gbuela.kanjiryokucha.model.Yomi;

import java.util.HashMap;

public class ReadingsManager implements LoadReadingsDelegate {

    private HashMap<Integer, Yomi> allReadings;
    private boolean loading;

    public static ReadingsManager instance = null;

    public static ReadingsManager getInstance() {
        if (instance == null) {
            instance = new ReadingsManager();
        }
        return instance;
    }

    private ReadingsManager() {
        allReadings = null;
        loading = false;
    }

    public synchronized void startLoadingReadings(Context context) {
        if (!loading && allReadings == null) {
            final LoadReadingsTask task = new LoadReadingsTask();
            task.execute(context, this);
            loading = true;
        }
    }

    @Override
    public synchronized void onLoadedReadings(HashMap<Integer, Yomi> allReadings) {
        this.allReadings = allReadings;
        loading = false;
    }

    public Yomi yomiForKanji(int kanjiId) {
        if(loading || allReadings == null) {
            return null;
        }

        return allReadings.get(kanjiId);
    }
}
